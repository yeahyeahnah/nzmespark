// Game variables
var SAFE_ZONE_WIDTH = 1280;
var SAFE_ZONE_HEIGHT = 720;
var game = new Phaser.Game(SAFE_ZONE_WIDTH, SAFE_ZONE_HEIGHT, Phaser.AUTO, 'desk-animation', {
	preload : preload,
	create : create,
	update : update
});

var imgMain;
var count;
var anim;

/* preload
 * Configure game
 * Preloads game assets
 */
function preload() {
	// *** GAME CONFIGURATION ***
	game.input.maxPointers = 1;
	game.stage.disableVisibilityChange = false;
	game.orientated = true;
	game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.scale.minWidth = game.width / 8;
	game.scale.minHeight = game.height / 8;
	game.scale.pageAlignHorizontally = true;
	game.scale.pageAlignVertically = true;
	if (game.device.desktop) {
		console.log("I'm on a desktop");
		game.scale.maxWidth = 1280;
		game.scale.maxHeight = 720;
	} else {
		console.log("I'm on a mobile");
		game.scale.maxWidth = 1280;
		game.scale.maxHeight = 720;
		game.scale.forceOrientation(true, false);
		game.scale.updateLayout(true);
	}

	// *** MENU PRELOAD ***
	game.stage.backgroundColor = '#ffffff';
	gameState = "Preload";
	game.load.spritesheet("1", "animations/images/desk/1280x720/1.jpg", 1280, 720, 1);
	game.load.spritesheet("2", "animations/images/desk/1280x720/2.jpg", 1280, 720);
	game.load.spritesheet("3", "animations/images/desk/1280x720/3.jpg", 1280, 720);
	game.load.spritesheet("4", "animations/images/desk/1280x720/4.jpg", 1280, 720);
	game.load.spritesheet("5", "animations/images/desk/1280x720/5.jpg", 1280, 720);
	game.load.spritesheet("6", "animations/images/desk/1280x720/6.jpg", 1280, 720);
	game.load.spritesheet("7", "animations/images/desk/1280x720/7.jpg", 1280, 720);
	game.load.spritesheet("8", "animations/images/desk/1280x720/8.jpg", 1280, 720);
	game.load.spritesheet("9", "animations/images/desk/1280x720/9.jpg", 1280, 720);
	game.load.spritesheet("10", "animations/images/desk/1280x720/10.jpg", 1280, 720);
	game.load.spritesheet("11", "animations/images/desk/1280x720/11.jpg", 1280, 720);
	game.load.spritesheet("12", "animations/images/desk/1280x720/12.jpg", 1280, 720);
	game.load.spritesheet("13", "animations/images/desk/1280x720/13.jpg", 1280, 720);
	game.load.spritesheet("14", "animations/images/desk/1280x720/14.jpg", 1280, 720);
	game.load.spritesheet("15", "animations/images/desk/1280x720/15.jpg", 1280, 720);
	game.load.spritesheet("16", "animations/images/desk/1280x720/16.jpg", 1280, 720);
	game.load.spritesheet("17", "animations/images/desk/1280x720/17.jpg", 1280, 720);
	game.load.spritesheet("18", "animations/images/desk/1280x720/18.jpg", 1280, 720);
	game.load.spritesheet("19", "animations/images/desk/1280x720/19.jpg", 1280, 720);
	game.load.spritesheet("20", "animations/images/desk/1280x720/20.jpg", 1280, 720);
	game.load.spritesheet("21", "animations/images/desk/1280x720/21.jpg", 1280, 720);
	game.load.spritesheet("22", "animations/images/desk/1280x720/22.jpg", 1280, 720);
	game.load.spritesheet("23", "animations/images/desk/1280x720/23.jpg", 1280, 720);
	game.load.spritesheet("24", "animations/images/desk/1280x720/24.jpg", 1280, 720);
	game.load.spritesheet("25", "animations/images/desk/1280x720/25.jpg", 1280, 720);
	game.load.spritesheet("26", "animations/images/desk/1280x720/26.jpg", 1280, 720);
	game.load.spritesheet("27", "animations/images/desk/1280x720/27.jpg", 1280, 720);
	game.load.spritesheet("28", "animations/images/desk/1280x720/28.jpg", 1280, 720);
	game.load.spritesheet("29", "animations/images/desk/1280x720/29.jpg", 1280, 720);
	game.load.spritesheet("30", "animations/images/desk/1280x720/30.jpg", 1280, 720);
	game.load.spritesheet("31", "animations/images/desk/1280x720/31.jpg", 1280, 720);
	game.load.spritesheet("32", "animations/images/desk/1280x720/32.jpg", 1280, 720);
	game.load.spritesheet("33", "animations/images/desk/1280x720/33.jpg", 1280, 720);
	game.load.spritesheet("34", "animations/images/desk/1280x720/34.jpg", 1280, 720);
	game.load.spritesheet("35", "animations/images/desk/1280x720/35.jpg", 1280, 720);
	game.load.spritesheet("36", "animations/images/desk/1280x720/36.jpg", 1280, 720);
	game.load.spritesheet("37", "animations/images/desk/1280x720/37.jpg", 1280, 720);
	game.load.spritesheet("38", "animations/images/desk/1280x720/38.jpg", 1280, 720);
	game.load.spritesheet("39", "animations/images/desk/1280x720/39.jpg", 1280, 720);
	game.load.spritesheet("40", "animations/images/desk/1280x720/40.jpg", 1280, 720);
	game.load.spritesheet("41", "animations/images/desk/1280x720/41.jpg", 1280, 720);
	game.load.spritesheet("42", "animations/images/desk/1280x720/42.jpg", 1280, 720);
	game.load.spritesheet("43", "animations/images/desk/1280x720/43.jpg", 1280, 720);
	game.load.spritesheet("44", "animations/images/desk/1280x720/44.jpg", 1280, 720);
	game.load.spritesheet("45", "animations/images/desk/1280x720/45.jpg", 1280, 720);
	game.load.spritesheet("46", "animations/images/desk/1280x720/46.jpg", 1280, 720);
	game.load.spritesheet("47", "animations/images/desk/1280x720/47.jpg", 1280, 720);
	game.load.spritesheet("48", "animations/images/desk/1280x720/48.jpg", 1280, 720);
	game.load.spritesheet("49", "animations/images/desk/1280x720/49.jpg", 1280, 720);
	game.load.spritesheet("50", "animations/images/desk/1280x720/50.jpg", 1280, 720);
	game.load.spritesheet("51", "animations/images/desk/1280x720/51.jpg", 1280, 720);
	game.load.spritesheet("52", "animations/images/desk/1280x720/52.jpg", 1280, 720);
	game.load.spritesheet("53", "animations/images/desk/1280x720/53.jpg", 1280, 720);
	game.load.spritesheet("54", "animations/images/desk/1280x720/54.jpg", 1280, 720);
	game.load.spritesheet("55", "animations/images/desk/1280x720/55.jpg", 1280, 720);
	game.load.spritesheet("56", "animations/images/desk/1280x720/56.jpg", 1280, 720);
	game.load.spritesheet("57", "animations/images/desk/1280x720/57.jpg", 1280, 720);
	game.load.spritesheet("58", "animations/images/desk/1280x720/58.jpg", 1280, 720);
}

/* create
 * Initial game setup
 */
function create() {
	imgMain = game.add.sprite(0, 0, "1");
	count = 0;
	anim = imgMain.animations.add("cycle");
    anim.onLoop.add(animationLooped, this);
	anim.onComplete.add(animationComplete, this);
	anim.play(20,true);
}

/* animationLooped
 * When a spritesheet is complete, call animationComplete
 */
function animationLooped(sprite, animation) {
	if(animation.loopCount === 1) {
		animationComplete(sprite, animation);
	}
}

/* animationComplete
 * Loads sequential textures from spritesheets
 */
function animationComplete(sprite, animation) {
	var currentCount = count + 2;
	var textureCount = currentCount.toString();
	console.log(textureCount);
	if(count == 56) {
		imgMain.loadTexture(textureCount);
		count = 0;
	} else {
		imgMain.loadTexture(textureCount);
		count++;
	}
	imgMain.animations.play("cycle", 20	, true);
}


/* update
 * Frequently called to update game state
 */
function update() {
	
}