// Game variables
var SAFE_ZONE_WIDTH = 1920;
var SAFE_ZONE_HEIGHT = 1080;
var game = new Phaser.Game(SAFE_ZONE_WIDTH, SAFE_ZONE_HEIGHT, Phaser.AUTO, 'mermaid-game', {
	preload : preload,
	create : create,
	update : update
});
// Asset variables
// Menu Assets
var imgSparkLogo, imgProgressBarBG, imgProgressBar, imgMenuBG, imgStar;
var btnPlay;
// Game Assets
var imgGameBG, imgGirlFG, imgBrush, imgBorderLeft, imgBorderRight;
var btnBrush, brushTween;
// Mermaid Assets
var ssMermaid, animMM, mmCount, animFrameRate;
// Progress Assets
var imgGameProgress;
var progTimer, progCounter;
// Win Assets
var imgTrophy, imgWinner;
var btnPlayAgain;
// Audio
var audioBG, audioClick, audioBrushing, audioWin;
// State control
var gameState;


/* preload
 * Configure game
 * Preloads game assets
 */
function preload() {
	// *** GAME CONFIGURATION ***
	game.input.maxPointers = 1;
	game.stage.disableVisibilityChange = false;
	game.orientated = true;
	game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.scale.minWidth = game.width / 8;
	game.scale.minHeight = game.height / 8;
	game.scale.pageAlignHorizontally = true;
	game.scale.pageAlignVertically = true;
	if (game.device.desktop) {
		console.log("I'm on a desktop");
		game.scale.maxWidth = 1920;
		game.scale.maxHeight = 1080;
	} else {
		console.log("I'm on a mobile");
		game.scale.maxWidth = 1920;
		game.scale.maxHeight = 1080;
		game.scale.forceOrientation(true, false);
		game.scale.updateLayout(true);
	}
	// *** BOOT SCREEN ***
	//game.stage.backgroundColor = '#ffffff';
	//game.load.image("imgSparkLogo", "images/1920x1080/menu_sprites/sparklogo.png");
	//game.load.image("imgProgressBarBG", "images/1920x1080/menu_sprites/progressbar_bg.png");
	//game.load.image("imgProgressBar", "images/1920x1080/menu_sprites/progressbar.png");

	// *** MENU PRELOAD ***
	game.stage.backgroundColor = '#ffffff';
	gameState = "Preload";
	// Preload Images
	game.load.image("imgMenuBG", "images/1920x1080/menu_sprites/menubg.png");
	game.load.image("imgStar", "images/1920x1080/menu_sprites/star.png");
	game.load.image("imgBtnPlayOff", "images/1920x1080/menu_sprites/btnplay-off.png");
	game.load.image("imgBtnPlayOn", "images/1920x1080/menu_sprites/btnplay-on.png");
	// Preload Audio Files
	game.load.audio("audioClick", "audio/button.mp3", "audio/button.ogg");
	
	// *** GAME PRELOAD ***
	// Show progress bar
	//game.load.setPreloadSprite(imgProgressBar);
	// Preload Images
	game.load.image("imgGameBG", "images/1920x1080/game_sprites/gamebg.png");
	game.load.image("imgGirlFG", "images/1920x1080/game_sprites/girl-fg.png");
	game.load.image("imgBrush", "images/1920x1080/game_sprites/brush.png");
	game.load.image("imgMermaid", "images/1920x1080/game_sprites/mermaid.png");
	game.load.image("imgBtnBrushOff", "images/1920x1080/game_sprites/btnbrush-off.png");
	game.load.image("imgBtnBrushOn", "images/1920x1080/game_sprites/btnbrush-on.png");
	game.load.image("imgBorderLeft", "images/1920x1080/game_sprites/border-left.png");
	game.load.image("imgBorderRight", "images/1920x1080/game_sprites/border-right.png");
	// Preload Spritesheets
	//  Mermaid Animations
	game.load.spritesheet("ssMM1", "images/1920x1080/mm_sprites/mm01.png", 310, 630, 4);
	game.load.spritesheet("ssMM2", "images/1920x1080/mm_sprites/mm02.png", 310, 630);
	game.load.spritesheet("ssMM3", "images/1920x1080/mm_sprites/mm03.png", 310, 630);
	game.load.spritesheet("ssMM4", "images/1920x1080/mm_sprites/mm04.png", 310, 630);
	game.load.spritesheet("ssMM5", "images/1920x1080/mm_sprites/mm05.png", 310, 630);
	game.load.spritesheet("ssMM6", "images/1920x1080/mm_sprites/mm06.png", 310, 630);
	game.load.spritesheet("ssMM7", "images/1920x1080/mm_sprites/mm07.png", 310, 630);
	game.load.spritesheet("ssMM8", "images/1920x1080/mm_sprites/mm08.png", 310, 630);
	game.load.spritesheet("ssMM9", "images/1920x1080/mm_sprites/mm09.png", 310, 630);
	game.load.spritesheet("ssMM10", "images/1920x1080/mm_sprites/mm10.png", 310, 630);
	game.load.spritesheet("ssMM11", "images/1920x1080/mm_sprites/mm11.png", 310, 630);
	game.load.spritesheet("ssMM12", "images/1920x1080/mm_sprites/mm12.png", 310, 630);
	game.load.spritesheet("ssMM13", "images/1920x1080/mm_sprites/mm13.png", 310, 630);
	game.load.spritesheet("ssMM14", "images/1920x1080/mm_sprites/mm14.png", 310, 630);
	game.load.spritesheet("ssMM15", "images/1920x1080/mm_sprites/mm15.png", 310, 630);
	game.load.spritesheet("ssMM16", "images/1920x1080/mm_sprites/mm16.png", 310, 630);
	game.load.spritesheet("ssMM17", "images/1920x1080/mm_sprites/mm17.png", 310, 630);
	game.load.spritesheet("ssMM18", "images/1920x1080/mm_sprites/mm18.png", 310, 630);
	game.load.spritesheet("ssMM19", "images/1920x1080/mm_sprites/mm19.png", 310, 630);
	game.load.spritesheet("ssMM20", "images/1920x1080/mm_sprites/mm20.png", 310, 630);
	// Preload Progress Images
	game.load.image("p0", "images/1920x1080/progress_sprites/000.png");
	game.load.image("p1", "images/1920x1080/progress_sprites/001.png");
	game.load.image("p2", "images/1920x1080/progress_sprites/002.png");
	game.load.image("p3", "images/1920x1080/progress_sprites/003.png");
	game.load.image("p4", "images/1920x1080/progress_sprites/004.png");
	game.load.image("p5", "images/1920x1080/progress_sprites/005.png");
	game.load.image("p6", "images/1920x1080/progress_sprites/006.png");
	game.load.image("p7", "images/1920x1080/progress_sprites/007.png");
	game.load.image("p8", "images/1920x1080/progress_sprites/008.png");
	game.load.image("p9", "images/1920x1080/progress_sprites/009.png");
	game.load.image("p10", "images/1920x1080/progress_sprites/010.png");
	game.load.image("p11", "images/1920x1080/progress_sprites/011.png");
	game.load.image("p12", "images/1920x1080/progress_sprites/012.png");
	game.load.image("p13", "images/1920x1080/progress_sprites/013.png");
	game.load.image("p14", "images/1920x1080/progress_sprites/014.png");
	game.load.image("p15", "images/1920x1080/progress_sprites/015.png");
	game.load.image("p16", "images/1920x1080/progress_sprites/016.png");
	game.load.image("p17", "images/1920x1080/progress_sprites/017.png");
	game.load.image("p18", "images/1920x1080/progress_sprites/018.png");
	game.load.image("p19", "images/1920x1080/progress_sprites/019.png");
	game.load.image("p20", "images/1920x1080/progress_sprites/020.png");
	game.load.image("p21", "images/1920x1080/progress_sprites/021.png");
	game.load.image("p22", "images/1920x1080/progress_sprites/022.png");
	game.load.image("p23", "images/1920x1080/progress_sprites/023.png");
	game.load.image("p24", "images/1920x1080/progress_sprites/024.png");
	game.load.image("p25", "images/1920x1080/progress_sprites/025.png");
	game.load.image("p26", "images/1920x1080/progress_sprites/026.png");
	game.load.image("p27", "images/1920x1080/progress_sprites/027.png");
	game.load.image("p28", "images/1920x1080/progress_sprites/028.png");
	game.load.image("p29", "images/1920x1080/progress_sprites/029.png");
	game.load.image("p30", "images/1920x1080/progress_sprites/030.png");
	game.load.image("p31", "images/1920x1080/progress_sprites/031.png");
	game.load.image("p32", "images/1920x1080/progress_sprites/032.png");
	game.load.image("p33", "images/1920x1080/progress_sprites/033.png");
	game.load.image("p34", "images/1920x1080/progress_sprites/034.png");
	game.load.image("p35", "images/1920x1080/progress_sprites/035.png");
	game.load.image("p36", "images/1920x1080/progress_sprites/036.png");
	game.load.image("p37", "images/1920x1080/progress_sprites/037.png");
	game.load.image("p38", "images/1920x1080/progress_sprites/038.png");
	game.load.image("p39", "images/1920x1080/progress_sprites/039.png");
	game.load.image("p40", "images/1920x1080/progress_sprites/040.png");
	game.load.image("p41", "images/1920x1080/progress_sprites/041.png");
	game.load.image("p42", "images/1920x1080/progress_sprites/042.png");
	game.load.image("p43", "images/1920x1080/progress_sprites/043.png");
	game.load.image("p44", "images/1920x1080/progress_sprites/044.png");
	game.load.image("p45", "images/1920x1080/progress_sprites/045.png");
	game.load.image("p46", "images/1920x1080/progress_sprites/046.png");
	game.load.image("p47", "images/1920x1080/progress_sprites/047.png");
	game.load.image("p48", "images/1920x1080/progress_sprites/048.png");
	game.load.image("p49", "images/1920x1080/progress_sprites/049.png");
	game.load.image("p50", "images/1920x1080/progress_sprites/050.png");
	game.load.image("p51", "images/1920x1080/progress_sprites/051.png");
	game.load.image("p52", "images/1920x1080/progress_sprites/052.png");
	game.load.image("p53", "images/1920x1080/progress_sprites/053.png");
	game.load.image("p54", "images/1920x1080/progress_sprites/054.png");
	game.load.image("p55", "images/1920x1080/progress_sprites/055.png");
	game.load.image("p56", "images/1920x1080/progress_sprites/056.png");
	game.load.image("p57", "images/1920x1080/progress_sprites/057.png");
	game.load.image("p58", "images/1920x1080/progress_sprites/058.png");
	game.load.image("p59", "images/1920x1080/progress_sprites/059.png");
	game.load.image("p60", "images/1920x1080/progress_sprites/060.png");
	game.load.image("p61", "images/1920x1080/progress_sprites/061.png");
	game.load.image("p62", "images/1920x1080/progress_sprites/062.png");
	game.load.image("p63", "images/1920x1080/progress_sprites/063.png");
	game.load.image("p64", "images/1920x1080/progress_sprites/064.png");
	game.load.image("p65", "images/1920x1080/progress_sprites/065.png");
	game.load.image("p66", "images/1920x1080/progress_sprites/066.png");
	game.load.image("p67", "images/1920x1080/progress_sprites/067.png");
	game.load.image("p68", "images/1920x1080/progress_sprites/068.png");
	game.load.image("p69", "images/1920x1080/progress_sprites/069.png");
	game.load.image("p70", "images/1920x1080/progress_sprites/070.png");
	game.load.image("p71", "images/1920x1080/progress_sprites/071.png");
	game.load.image("p72", "images/1920x1080/progress_sprites/072.png");
	game.load.image("p73", "images/1920x1080/progress_sprites/073.png");
	game.load.image("p74", "images/1920x1080/progress_sprites/074.png");
	game.load.image("p75", "images/1920x1080/progress_sprites/075.png");
	game.load.image("p76", "images/1920x1080/progress_sprites/076.png");
	game.load.image("p77", "images/1920x1080/progress_sprites/077.png");
	game.load.image("p78", "images/1920x1080/progress_sprites/078.png");
	game.load.image("p79", "images/1920x1080/progress_sprites/079.png");
	game.load.image("p80", "images/1920x1080/progress_sprites/080.png");
	game.load.image("p81", "images/1920x1080/progress_sprites/081.png");
	game.load.image("p82", "images/1920x1080/progress_sprites/082.png");
	game.load.image("p83", "images/1920x1080/progress_sprites/083.png");
	game.load.image("p84", "images/1920x1080/progress_sprites/084.png");
	game.load.image("p85", "images/1920x1080/progress_sprites/085.png");
	game.load.image("p86", "images/1920x1080/progress_sprites/086.png");
	game.load.image("p87", "images/1920x1080/progress_sprites/087.png");
	game.load.image("p88", "images/1920x1080/progress_sprites/088.png");
	game.load.image("p89", "images/1920x1080/progress_sprites/089.png");
	game.load.image("p90", "images/1920x1080/progress_sprites/090.png");
	game.load.image("p91", "images/1920x1080/progress_sprites/091.png");
	game.load.image("p92", "images/1920x1080/progress_sprites/092.png");
	game.load.image("p93", "images/1920x1080/progress_sprites/093.png");
	game.load.image("p94", "images/1920x1080/progress_sprites/094.png");
	game.load.image("p95", "images/1920x1080/progress_sprites/095.png");
	game.load.image("p96", "images/1920x1080/progress_sprites/096.png");
	game.load.image("p97", "images/1920x1080/progress_sprites/097.png");
	game.load.image("p98", "images/1920x1080/progress_sprites/098.png");
	game.load.image("p99", "images/1920x1080/progress_sprites/099.png");
	game.load.image("p100", "images/1920x1080/progress_sprites/100.png");
	// Preload Audio Files
	game.load.audio("audioBG", "audio/bgmusic.mp3", "audio/bgmusic.ogg");
	game.load.audio("audioBrushing", "audio/brushing.mp3", "audio/brushing.ogg");
	
	// *** WIN PRELOAD ***
	game.load.image("imgTrophy", "images/1920x1080/win_sprites/trophy.png");
	game.load.image("imgWinner", "images/1920x1080/win_sprites/win.png");
	game.load.image("imgBtnPlayAgainOff", "images/1920x1080/win_sprites/playagain-off.png");
	game.load.image("imgBtnPlayAgainOn", "images/1920x1080/win_sprites/playagain-on.png");
	// Preload Audio Files
	game.load.audio("audioWin", "audio/win.mp3", "audio/win.ogg");
}

/* create
 * Initial game setup
 */
function create() {
	//imgSparkLogo = game.add.sprite(game.width / 2, game.height / 2, "imgSparkLogo");
	//imgSparkLogo.anchor.setTo(0.5);
	//imgProgressBarBG = game.add.sprite(game.width / 2, (game.height / 2) + 110, "imgProgressBarBG");
	//imgProgressBarBG.anchor.setTo(0.5);
	//imgProgressBar = game.add.sprite(game.width / 2, (game.height / 2) + 110, "imgProgressBar");
	//imgProgressBar.anchor.setTo(0.5);
	// Animate Progress Bar
	//game.load.setPreloadSprite(imgProgressBar);
	
	// *** MAIN MENU ***
	gameState = "Menu";
	// Hide Boot Screen
	//imgSparkLogo.visible = false;
	//imgProgressBarBG.visible = false;
	//imgProgressBar.visible = false;
	// Menu Screen
	imgMenuBG = game.add.sprite(0, 0, "imgMenuBG");
	imgStar = game.add.sprite(-338, (game.height / 2), "imgStar");
	imgStar.anchor.setTo(0.505,0.505);
	imgStar.scale.setTo(1.5);
	btnPlay = game.add.button(-338, (game.height / 2), "imgBtnPlayOff", actionStartGame, this, 1, 0, 0);
	btnPlay.anchor.setTo(0.5);
	btnPlay.onInputOver.add(playOver, this);
	btnPlay.onInputOut.add(playOut, this);
	// Initialize Audio Files
	audioClick = game.add.audio("audioClick");
	
	// *** GAME SCREEN ***
	//  Game Images
	imgGameBG = game.add.sprite(0, 0, "imgGameBG");
	imgBorderLeft = game.add.sprite(0, 0, "imgBorderLeft");
	imgBrush = game.add.sprite(0, 554, "imgBrush");
	imgMermaid = game.add.sprite(0, 0, "imgMermaid");
	//  Mermaid Spritesheet
	ssMermaid = game.add.sprite(1250, 0, "ssMM1");
	mmCount = 0;
	animFrameRate = 60;
	// Progress Animation
	imgGameProgress = game.add.sprite(990, 278, "p0");
	//progTimer = new Timer(game, false);
	progCounter = 0;
	//  Foreground Images
	btnBrush = game.add.button(this.game.width + 125, this.game.height - 200, "imgBtnBrushOff", actionBrushTeeth, this, 1, 0, 0);
	btnBrush.anchor.setTo(0.5);
	btnBrush.onInputOver.add(brushOver, this);
	btnBrush.onInputOut.add(brushOut, this);
	imgBorderRight = game.add.sprite(215, 0, "imgBorderRight");
	imgGirlFG = game.add.sprite(0, 0, "imgGirlFG");
	// Inialize Audio Files
	audioBG = game.add.audio("audioBG");
	audioBrushing = this.game.add.audio("audioBrushing");
	
	
	// *** WIN SCREEN ***
	imgTrophy = game.add.sprite(game.width / 2, game.height / 2, "imgTrophy");
	imgTrophy.anchor.setTo(0.5);
	imgWinner = game.add.sprite(game.width / 2, game.height / 2, "imgWinner");
	imgWinner.anchor.setTo(0.5);
	//btnPlayAgain = game.add.sprite(game.width / 2, game.height / 2 + 150, "imgBtnPlayAgainOff", actionRestartGame, this, 1, 0, 0);
	//btnPlayAgain.anchor.setTo(0.5);
	//btnPlayAgain.onInputOver.add(playAgainOver, this);
	//btnPlayAgain.onInputOut.add(playAgainOut, this);
	audioWin = this.game.add.audio("audioWin");
	
	// Hide Game Assets
	viewGameAssets(0);
}

/* actionStartGame
 * Calls startGame to initiate gameplay
 */
function actionStartGame() {
	audioClick.play();
	btnPlay.visible = false;
	imgStar.visible = false;
	startGame();
}

/* startGame
 * Changes game state from the menu to running
 */
function startGame() {
	// Change game state
	gameState = "Starting";
	// Start BG music
	audioBG.loopFull();
	// Create mermaid animation
	animMM = ssMermaid.animations.add("cycle");
    animMM.onLoop.add(animationLooped, this);
	animMM.onComplete.add(animationComplete, this);
	// Hide Progress Bar
	//imgProgressBarBG.visible = false;
	//imgProgressBar.visible = false;
	imgMenuBG.visible = false;
	// Reveal game assets
	viewGameAssets(1);
}

/* actionBrushTeeth
 * Called from Brush button
 * Starts mermaid animation
 * Starts brushing animation
 */
function actionBrushTeeth() {
	gameState = "Playing";
	animMM.play(animFrameRate,true);
	brushTween = game.add.tween(imgBrush)
		.to({ x: -65, y: 535 }, 1000, Phaser.Easing.Linear.None)
		.to({ x: 0, y: 554 }, 1000, Phaser.Easing.Linear.None).loop(true).start();
	audioBrushing.loopFull();
	btnBrush.visible = false;
	wobbleGameProgress();
	startTimer();
}

/* wobbleGameProgress
 * Animates the progress image to appear to be wobbling
 */
function wobbleGameProgress() {
	var progTween = game.add.tween(imgGameProgress)
	.to({ x: 985, y: 268 }, 1000, Phaser.Easing.Linear.None)
	.to({ x: 995, y: 258 }, 1500, Phaser.Easing.Linear.None)
	.to({ x: 985, y: 248 }, 1500, Phaser.Easing.Linear.None)
	.to({ x: 990, y: 238 }, 1000, Phaser.Easing.Linear.None)
	.to({ x: 995, y: 248 }, 1000, Phaser.Easing.Linear.None)
	.to({ x: 985, y: 258 }, 1500, Phaser.Easing.Linear.None)
	.to({ x: 995, y: 268 }, 1500, Phaser.Easing.Linear.None)
	.to({ x: 990, y: 278 }, 1000, Phaser.Easing.Linear.None).loop(true).start();
}

/* animationLooped
 * When a spritesheet is complete, call animationComplete
 */
function animationLooped(sprite, animation) {
	if(animation.loopCount === 1) {
		animationComplete(sprite, animation);
	}
}

/* animationComplete
 * Loads sequential textures from spritesheets
 */
function animationComplete(sprite, animation) {
	if(mmCount == 0) {
		ssMermaid.loadTexture("ssMM2");
		mmCount++;
	} else if(mmCount == 1) {
		ssMermaid.loadTexture("ssMM3");
		mmCount++;
	} else if(mmCount == 2) {
		ssMermaid.loadTexture("ssMM4");
		mmCount++;
	} else if(mmCount == 3) {
		ssMermaid.loadTexture("ssMM5");
		mmCount++;
	} else if(mmCount == 4) {
		ssMermaid.loadTexture("ssMM6");
		mmCount++;
	} else if(mmCount == 5) {
		ssMermaid.loadTexture("ssMM7");
		mmCount++;
	} else if(mmCount == 6) {
		ssMermaid.loadTexture("ssMM8");
		mmCount++;
	} else if(mmCount == 7) {
		ssMermaid.loadTexture("ssMM9");
		mmCount++;
	} else if(mmCount == 8) {
		ssMermaid.loadTexture("ssMM10");
		mmCount++;
	} else if(mmCount == 9) {
		ssMermaid.loadTexture("ssMM11");
		mmCount++;
	} else if(mmCount == 10) {
		ssMermaid.loadTexture("ssMM12");
		mmCount++;
	} else if(mmCount == 11) {
		ssMermaid.loadTexture("ssMM13");
		mmCount++;
	} else if(mmCount == 12) {
		ssMermaid.loadTexture("ssMM14");
		mmCount++;
	} else if(mmCount == 13) {
		ssMermaid.loadTexture("ssMM15");
		mmCount++;
	} else if(mmCount == 14) {
		ssMermaid.loadTexture("ssMM16");
		mmCount++;
	} else if(mmCount == 15) {
		ssMermaid.loadTexture("ssMM17");
		mmCount++;
	} else if(mmCount == 16) {
		ssMermaid.loadTexture("ssMM18");
		mmCount++;
	} else if(mmCount == 17) {
		ssMermaid.loadTexture("ssMM19");
		mmCount++;
	} else if(mmCount == 18) {
		ssMermaid.loadTexture("ssMM20");
		mmCount++;
		//mmCount = 0;
	} else if(mmCount == 19) {
		ssMermaid.loadTexture("ssMM1");
		mmCount = 0;
	}
	ssMermaid.animations.play("cycle", animFrameRate, true);
}

/* playOver
 * Logs to console that button over initiated
 * Performs hover image on mouse over
 */
function playOver() {
	audioClick.play();
	game.add.tween(imgStar.scale).to({
		x : 1.9,
		y : 1.9
	}, 100, Phaser.Easing.Linear.None, true);
	game.add.tween(btnPlay.scale).to({
		x : 1.2,
		y : 1.2
	}, 100, Phaser.Easing.Linear.None, true);
	btnPlay.loadTexture("imgBtnPlayOn");
}

/* playOut
 * Logs to console that button out initiated
 * Scales star on button mouse over
 */
function playOut() {
	game.add.tween(imgStar.scale).to({
		x : 1.5,
		y : 1.5
	}, 100, Phaser.Easing.Linear.None, true);
	game.add.tween(btnPlay.scale).to({
		x : 1,
		y : 1
	}, 100, Phaser.Easing.Linear.None, true);
	btnPlay.loadTexture("imgBtnPlayOff");
}

/* brushOver
 * Logs to console that button over initiated
 * Loads brush over image
 * Scales brush up
 */
function brushOver() {
	audioClick.play();
	game.add.tween(btnBrush.scale).to({
		x : 1.2,
		y : 1.2
	}, 100, Phaser.Easing.Linear.None, true);
	btnBrush.loadTexture("imgBtnBrushOn");
}

/* brushOut
 * Logs to console that button out initiated
 * Loads brush off image
 * Scales brush back down
 */
function brushOut() {
	game.add.tween(btnBrush.scale).to({
		x : 1,
		y : 1
	}, 100, Phaser.Easing.Linear.None, true);
	btnBrush.loadTexture("imgBtnBrushOff");
}

/* playAgainOver
 * Logs to console that button over initiated
 * Loads brush over image
 * Scales brush up
 */
/*function playAgainOver() {
	audioClick.play();
	game.add.tween(btnPlayAgain.scale).to({
		x : 1.2,
		y : 1.2
	}, 100, Phaser.Easing.Linear.None, true);
	btnPlayAgain.loadTexture("imgBtnPlayAgainOn");
}

/* playAgainOut
 * Logs to console that button out initiated
 * Loads brush off image
 * Scales brush back down
 */
/*function playAgainOut() {
	game.add.tween(btnPlayAgain.scale).to({
		x : 1,
		y : 1
	}, 100, Phaser.Easing.Linear.None, true);
	btnBrush.loadTexture("imgBtnPlayAgainOff");
}*/

/* startTimer
 * Starts the progress timer to increment progress
 */
function startTimer() {
	console.log("Progress: " + progCounter + "%");
	game.time.events.loop(250, incProgressCounter, this);
}

/* incProgressCounter
 * Increments progress counter accorinding to timer
 */
function incProgressCounter() {
	if(progCounter < 100) {
		progCounter++;
		//console.log("Progress: " + progCounter + "%");
	} else {
		game.time.events.stop();
		animMM.stop();
		brushTween.stop();
		winGame();
	}
}

/* viewGameAssets
 * Method to reuse the show/hide code for game assets
 */
function viewGameAssets(e) {
	if(e == 0) { // hide all
		imgGameBG.visible = false;
		imgGameProgress.visible = false;
		imgGirlFG.visible = false;
		imgBrush.visible = false;
		imgMermaid.visible = false;
		ssMermaid.visible = false;
		imgBorderLeft.visible = false;
		imgBorderRight.visible = false;
		btnBrush.visible = false;
		imgTrophy.visible = false;
		imgWinner.visible = false;
		//btnPlayAgain.visible = false;
	} else if(e == 1) { // show all
		imgGameBG.visible = true;
		imgGameProgress.visible = true;
		imgGirlFG.visible = true;
		imgBrush.visible = true;
		imgMermaid.visible = true;
		ssMermaid.visible = true;
		btnBrush.visible = true;
		imgBorderRight.visible = true;
		imgBorderLeft.visible = true;
	} else if(e == 2) { // show win
		imgMenuBG.visible = true;
		imgTrophy.visible = true;
		imgWinner.visible = true;
		//btnPlayAgain.visible = true;
	} else {
		console.log("viewGameAssets: Invalid argument given.");
	}
}

/* winGame
 * GG
 */
function winGame() {
	console.log("You won!!!");
	audioBG.stop();
	audioBrushing.stop();
	// Hide Game Assets
	viewGameAssets(0);
	viewGameAssets(2);
	audioWin.play();
}


function actionRestartGame() {
	viewGameAssets(0);
	startGame();
}

/* update
 * Frequently called to update game state
 */
function update() {
	// Update Menu State
	if (gameState == "Menu") {
		// Slide controls onto screen
		if (imgStar.x < (game.width / 2) + 1) {
			imgStar.x += 20;
		} else {
			imgStar.angle += 2;
		}
		if (btnPlay.x < (game.width / 2)) {
			btnPlay.x += 20;
		}
	}
	// Update Starting State
	if (gameState == "Starting") {
		// Slide controls onto screen
		if (btnBrush.x > (game.width / 2)) {
			btnBrush.x -= 25;
		}
	}
	// Update Playing State
	if(gameState == "Playing") {
		// Update the progress image
		imgGameProgress.loadTexture("p" + progCounter);
	}
}